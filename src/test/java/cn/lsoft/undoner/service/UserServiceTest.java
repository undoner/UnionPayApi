package cn.lsoft.undoner.service;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import cn.lsoft.undoner.baseTest.SpringTestCase;
import cn.lsoft.undoner.dao.domain.User;
import cn.lsoft.undoner.service.user.UserService;

/**
 * @author Administrator
 *
 */
public class UserServiceTest extends SpringTestCase	{
	@Resource
	private UserService userService;
	Logger logger = Logger.getLogger(UserServiceTest.class);
	
	@Test
	public void testByTest(){
        logger.error(System.currentTimeMillis());
	}
	
	@Test
	public void testSelectUserById(){
		User user = userService.selectUserById(1);
        logger.error("查找结果" + user);
	}
	
	@Test
	public void testUpdateUserById(){
		int user = userService.updateUserById(1);
        logger.error("查找结果" + user);
	}
	
	@Test
	public void testAddUser(){
		int user = userService.addUser();
        logger.error("查找结果" + user);
	}
	
	@Test
	public void testDelUserById(){
		int user = userService.delUserById(2);
        logger.error("查找结果" + user);
	}
}
