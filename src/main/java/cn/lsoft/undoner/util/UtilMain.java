package cn.lsoft.undoner.util;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import cn.lsoft.undoner.model.pay.MerchantData;

public class UtilMain {

	public static void main(String[] args) {
		String a_token="123";
		Hashtable<String,MerchantData> merchantCache = MerchantCache.getInstance().merchantCacheHash;
		MerchantData merchantMessage = new MerchantData();
		merchantMessage.setMerchantId("123");
		merchantCache.put(a_token, merchantMessage);
		
		for (Map.Entry<String, MerchantData> entry : merchantCache.entrySet()) {
		    System.out.println("put	: Key = " + entry.getKey() + ", Value = " + entry.getValue().toString());
		}
		
		if(merchantCache.containsKey(a_token)){
			merchantMessage = merchantCache.get(a_token);
		}else{
			//doSomeThing();
			System.out.println("请求丢失:"+a_token);
		}
		
	}

}
