package cn.lsoft.undoner.util;

import java.util.Hashtable;

import cn.lsoft.undoner.model.pay.MerchantData;

/** 
* @ClassName: MerchantCache 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author undoner@gmail.com
* @date 2017年2月8日 下午4:49:56 
*/
public class MerchantCache {
	public Hashtable<String,MerchantData> merchantCacheHash = new Hashtable<String,MerchantData>();
	private static MerchantCache merchantCache = null;
	
	/** 
	* @Title: getInstance 
	* @Description: TODO(单例，模拟缓存数据) 
	* @param @return    设定参数 
	* @return MerchantCache    返回类型 
	* @throws 
	* @author undoner@gmail.com 
	* @date 2017年2月8日 下午4:49:59 
	*/
	public static MerchantCache getInstance(){
		if(merchantCache==null){
			merchantCache= new MerchantCache();
		}
		return merchantCache;
	}

}
