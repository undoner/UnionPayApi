package cn.lsoft.undoner.service.pay;

import java.beans.IntrospectionException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;

import cn.lsoft.undoner.model.pay.MerchantData;
import cn.lsoft.undoner.model.pay.QrcodeRetData;

/** 
* @ClassName: PayService 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author undoner@gmail.com
* @date 2017年2月7日 下午5:04:08 
*/
public interface PayService {
	/** 
	* @Title: acceptPay 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param merchantMessage
	* @param @return    设定参数 
	* @return String    返回类型 
	* @throws 
	* @author undoner@gmail.com 
	* @date 2017年2月7日 下午5:04:05 
	*/
	QrcodeRetData acceptPay(MerchantData merchantMessage);

	/** 
	* @Title: validMd5Parameter 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param merchantMessage
	* @param @return    设定参数 
	* @return boolean    返回类型 
	 * @throws IntrospectionException 
	 * @throws UnsupportedEncodingException 
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	* @throws 
	* @author undoner@gmail.com 
	* @date 2017年2月8日 下午12:57:42 
	*/
	boolean isValidMd5Parameter(MerchantData merchantMessage) throws IllegalAccessException, InvocationTargetException, UnsupportedEncodingException, IntrospectionException;


	/** 
	* @Title: unionPay 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param merchantMessage
	* @param @return    设定参数 
	* @return String    返回类型 
	* @throws 
	* @author undoner@gmail.com 
	* @date 2017年2月8日 下午4:31:29 
	*/
	String unionPay(MerchantData merchantMessage);
	
	/** 
	* @Title: acceptOrder 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param a_token
	* @param @return    设定参数 
	* @return MerchantMessage    返回类型 
	* @throws 
	* @author undoner@gmail.com 
	* @date 2017年2月8日 下午4:09:52 
	*/
	MerchantData acceptOrder(String a_token);

}
