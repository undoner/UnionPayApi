package cn.lsoft.undoner.service.user.impl;

import java.util.Date;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import cn.lsoft.undoner.dao.domain.User;
import cn.lsoft.undoner.dao.mapper.UserMapper;
import cn.lsoft.undoner.service.user.UserService;

/** 
* @ClassName: UserServiceImpl 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author undoner@gmail.com
* @date 2017年1月21日 下午1:06:29 
*/
@Service
public class UserServiceImpl implements UserService{
	private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
	@Resource
	private UserMapper userMapper;

	public User selectUserById(Integer userId) {
		return userMapper.selectByPrimaryKey(userId);
		
	}

	@Override
	public int updateUserById(Integer userId) {
		User user = new User();
		user.setId(userId);
		user.setUserDescription(new Date().toString());
		return userMapper.updateByPrimaryKeySelective(user);
	}

	@Override
	public int addUser() {
		String data = String.valueOf(System.currentTimeMillis());
		User user = new User();
		user.setUserName(data);
		user.setUserType(1);
		user.setUserPassword("12345");
		user.setUserDescription(data);
		user.setUserEmail(data+"@lsoft.cn");
		user.setUserDescription(new Date().toString());
		return userMapper.insertSelective(user);
	}

	@Override
	public int delUserById(Integer userId) {
		return userMapper.deleteByPrimaryKey(userId);
	}

}
