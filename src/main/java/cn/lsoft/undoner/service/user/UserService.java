package cn.lsoft.undoner.service.user;

import org.springframework.stereotype.Service;

import cn.lsoft.undoner.dao.domain.User;


/**
 * 功能概要：UserService接口类
 * 
 * @author linbingwen
 * @since  2015年9月28日 
 */
public interface UserService {
	/** 
	* @Title: selectUserById 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param userId
	* @param @return    设定参数 
	* @return User    返回类型 
	* @throws 
	* @author undoner@gmail.com 
	* @date 2017年1月21日 下午1:17:12 
	*/
	User selectUserById(Integer userId);
	
	/** 
	* @Title: updateUserById 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param userId
	* @param @return    设定参数 
	* @return int    返回类型 
	* @throws 
	* @author undoner@gmail.com 
	* @date 2017年1月21日 下午1:17:15 
	*/
	int updateUserById(Integer userId);
	
	/** 
	* @Title: addUser 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @return    设定参数 
	* @return int    返回类型 
	* @throws 
	* @author undoner@gmail.com 
	* @date 2017年1月21日 下午7:45:19 
	*/
	int addUser();
	
	/** 
	* @Title: delUserById 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param userId
	* @param @return    设定参数 
	* @return int    返回类型 
	* @throws 
	* @author undoner@gmail.com 
	* @date 2017年1月21日 下午7:45:21 
	*/
	int delUserById(Integer userId);

}
