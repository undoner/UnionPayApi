package cn.lsoft.undoner.model.view;

public class JsonResponse {
	private String requestId;
	private Error error;
	public String getRequestId() {
		return requestId;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	
	public void setError(int code, String message) {
		if(null == error){
			error = new Error();
		}
		error.setCode(code);
		error.setMessage(message);	
	}
	
	
}
class Error{
	private int code;
	private String message;
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
