package cn.lsoft.undoner.model.view;

/**
 * Created by ${<A HREF="mailto:undoner@gmail.com">undoner</A>} on 16-12-20.
 */
public class ResponseMessage {

    int code;
    String msg;
    Object object;

    public ResponseMessage() {
        this.code = 0;
        this.msg = "success";
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }
}
