package cn.lsoft.undoner.model.pay;

/** 
* @ClassName: QrcodeRetData 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author undoner@gmail.com
* @date 2017年2月8日 下午4:52:54 
*/
public class QrcodeRetData {
	private String successful;
	private String errorMsg;
	private String qrcode;
	private String qrcodeVal;
	public String getSuccessful() {
		return successful;
	}
	public void setSuccessful(String successful) {
		this.successful = successful;
	}
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	public String getQrcode() {
		return qrcode;
	}
	public void setQrcode(String qrcode) {
		this.qrcode = qrcode;
	}
	public String getQrcodeVal() {
		return qrcodeVal;
	}
	public void setQrcodeVal(String qrcodeVal) {
		this.qrcodeVal = qrcodeVal;
	}
	public static QrcodeRetData  makeFailRetData(String errorMsg) {
		QrcodeRetData failRetData = new QrcodeRetData();
		failRetData.setSuccessful("FAIL");
		failRetData.setErrorMsg(errorMsg);
		return failRetData;
	}
}
