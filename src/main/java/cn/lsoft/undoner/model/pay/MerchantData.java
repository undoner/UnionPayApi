package cn.lsoft.undoner.model.pay;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.lang.reflect.Field;

public class MerchantData implements Serializable {

	private String serviceName; // 接口名称
	private String merchantId; // 商户编号
	private String inputCharset; // 字符编码
	private String signType; // 签名类型
	private String signValue; // 签名
	private String returnURL; // 支付结果通知URL (部分接口使用此地址异步通知商户)
	private String errorURL; // 支付异常返回URL
	private String notifyURL; // 支付异步通知URL (只有带界面的处理需要异步通知才使用。)
	private String productName; // 商品名称
	private String tradeMoney; // 交易金额(分为单位)
	private String payType; // 支付类型
	private String remark; // 额外参数

	public Map<String, String> toMap() {
		Map<String, String> map = new HashMap<String, String>();
		Field[] fields = this.getClass().getDeclaredFields();
		for (Field field : fields) {
			Object obj;
			try {
				obj = field.get(this);
				if (obj != null && String.valueOf(obj).length() != 0) {
					map.put(field.getName(), String.valueOf(obj));
				}
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		return map;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getSignType() {
		return signType;
	}

	public void setSignType(String signType) {
		this.signType = signType;
	}

	public String getSignValue() {
		return signValue;
	}

	public void setSignValue(String signValue) {
		this.signValue = signValue;
	}

	public String getReturnURL() {
		return returnURL;
	}

	public void setReturnURL(String returnURL) {
		this.returnURL = returnURL;
	}

	public String getErrorURL() {
		return errorURL;
	}

	public void setErrorURL(String errorURL) {
		this.errorURL = errorURL;
	}

	public String getNotifyURL() {
		return notifyURL;
	}

	public void setNotifyURL(String notifyURL) {
		this.notifyURL = notifyURL;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getTradeMoney() {
		return tradeMoney;
	}

	public void setTradeMoney(String tradeMoney) {
		this.tradeMoney = tradeMoney;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	

	public String getInputCharset() {
		return inputCharset;
	}

	public void setInputCharset(String inputCharset) {
		this.inputCharset = inputCharset;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	@Override
	public String toString() {
		return "MerchantMessage [serviceName=" + serviceName + ", merchantId=" + merchantId + ", inputCharset="
				+ inputCharset + ", signType=" + signType + ", signValue=" + signValue + ", returnURL=" + returnURL
				+ ", errorURL=" + errorURL + ", notifyURL=" + notifyURL + ", productName=" + productName
				+ ", tradeMoney=" + tradeMoney + ", payType=" + payType + ", remark=" + remark + "]";
	}

}
