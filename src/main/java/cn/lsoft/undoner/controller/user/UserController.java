package cn.lsoft.undoner.controller.user;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.lsoft.undoner.controller.BaseController;
import cn.lsoft.undoner.dao.domain.User;
import cn.lsoft.undoner.model.view.ResponseMessage;
import cn.lsoft.undoner.service.user.UserService;
import cn.lsoft.undoner.service.user.impl.UserServiceImpl;


/** 
* @ClassName: UserController 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author undoner@gmail.com
* @date 2017年1月21日 下午7:42:32 
*/
@Controller
@RequestMapping("/user")  
public class UserController implements BaseController {
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	@Resource
	private UserService userService;
	private ResponseMessage responseMessage;
	
	/** 
	* @Title: getIndex 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @return    设定参数 
	* @return ModelAndView    返回类型 
	* @throws 
	* @author undoner@gmail.com 
	* @date 2017年1月21日 下午1:15:45 
	*/
	@RequestMapping("/")  
    public ModelAndView getIndex(){    
		ModelAndView mav = new ModelAndView("index"); 
		User user = userService.selectUserById(1);
	    mav.addObject("user", user); 
        return mav;  
    }  
	
	/** 
	* @Title: updateByUser 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @return    设定参数 
	* @return ResponseMessage    返回类型 
	* @throws 
	* @author undoner@gmail.com 
	* @date 2017年1月21日 下午1:15:38 
	*/
	@RequestMapping("/update")  
	@ResponseBody
    public ResponseMessage updateByUser(){    
		responseMessage=new ResponseMessage();
        try {
        	int user = userService.updateUserById(1);
            responseMessage.setCode(1);
            responseMessage.setMsg("success");
            responseMessage.setObject(user);
        } catch (Exception e) {
            responseMessage.setCode(-1);
            responseMessage.setMsg(e.getMessage());
        }
        return responseMessage;
    }  
	
	@RequestMapping("/addUser")  
	@ResponseBody
    public ResponseMessage addUser(){    
		responseMessage=new ResponseMessage();
        try {
        	int user = userService.addUser();
            responseMessage.setCode(1);
            responseMessage.setMsg("success");
            responseMessage.setObject(user);
        } catch (Exception e) {
            responseMessage.setCode(-1);
            responseMessage.setMsg(e.getMessage());
        }
        return responseMessage;
    }  
	
	@RequestMapping("/delUser")  
	@ResponseBody
    public ResponseMessage delUser(){    
		responseMessage=new ResponseMessage();
        try {
        	int user = userService.delUserById(1);
            responseMessage.setCode(1);
            responseMessage.setMsg("success");
            responseMessage.setObject(user);
        } catch (Exception e) {
            responseMessage.setCode(-1);
            responseMessage.setMsg(e.getMessage());
        }
        return responseMessage;
    }  
}
