package cn.lsoft.undoner.controller.pay;

import java.io.IOException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.annotation.Resource;
import javax.enterprise.inject.Model;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.lsoft.undoner.dao.domain.User;
import cn.lsoft.undoner.model.pay.MerchantData;
import cn.lsoft.undoner.model.pay.QrcodeRetData;
import cn.lsoft.undoner.model.view.ResponseMessage;
import cn.lsoft.undoner.service.pay.PayService;
import cn.lsoft.undoner.service.user.impl.UserServiceImpl;
import cn.lsoft.undoner.util.StringUtil;

/** 
* @ClassName: PayController 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author undoner@gmail.com
* @date 2017年2月7日 下午4:52:54 
*/
@RequestMapping("/pay") 
@Controller
public class PayController {
	private static final Logger logger = LoggerFactory.getLogger(PayController.class);
	@Resource
	private PayService payService;
	private ResponseMessage responseMessage;

	
	/** 
	* @Title: updateByUser 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @return    设定参数 
	* @return ResponseMessage    返回类型 
	 * @throws IOException 
	* @throws 
	* @author undoner@gmail.com 
	* @date 2017年2月7日 下午4:52:57 
	*/
	@RequestMapping("/gateway")  
	@ResponseBody
    public ResponseMessage acceptPay(MerchantData merchantMessage,HttpServletRequest request, HttpServletResponse response) throws IOException{    
		responseMessage=new ResponseMessage();
		QrcodeRetData qrcodeRetData = null;
        try {
        	//检查必传参数
        	//doSomeThing();
        	logger.debug(merchantMessage.toString());
        	
        	//校验MD5值
        	if(!payService.isValidMd5Parameter(merchantMessage)){
        		logger.info("签名验证失败");
        		responseMessage.setCode(0);
                responseMessage.setMsg("fail");
                responseMessage.setObject(merchantMessage);
        	}else{
        		qrcodeRetData = payService.acceptPay(merchantMessage);
                responseMessage.setCode(1);
                responseMessage.setMsg("success");
                responseMessage.setObject(qrcodeRetData);
        	}
        	
        } catch (Exception e) {
            responseMessage.setCode(-1);
            responseMessage.setMsg(e.getMessage());
        }
//        直接跳转到二维码内容的地址
//        response.sendRedirect(qrcodeRetData.getQrcodeVal());
        return responseMessage;
    }  
	
	/** 
	* @Title: unionPay 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @return    设定参数 
	* @return ResponseMessage    返回类型 
	* @throws 
	* @author undoner@gmail.com 
	* @date 2017年2月7日 下午5:03:54 
	*/
	@RequestMapping("/order")  
    public ModelAndView acceptOrder(HttpServletRequest request, HttpServletResponse response){    
		responseMessage=new ResponseMessage();
		MerchantData merchantMessage =null;
		Map<String,Object> data = new HashMap<String,Object>(); 
        try {
        	String a_token = request.getParameter("a_token");
        	
        	//判断参数
        	if (StringUtil.isEmptyString(a_token)){
        		logger.info("请求参数中 a_token 不存在");
        		responseMessage.setCode(0);
                responseMessage.setMsg("fail");
                responseMessage.setObject(null);
    			return new ModelAndView("payerror");
    		}else{
    			merchantMessage = payService.acceptOrder(a_token);
                responseMessage.setCode(1);
                responseMessage.setMsg("success");
                responseMessage.setObject(merchantMessage);
    		}
        	
        } catch (Exception e) {
            responseMessage.setCode(-1);
            responseMessage.setMsg(e.getMessage());
        }
        logger.info("merchantMessage:"+merchantMessage.toString());
        ModelAndView mav = new ModelAndView("paysubmit"); 
	    mav.addObject("merchantMessage", merchantMessage); 
        return mav;
    }  
	
	/** 
	* @Title: unionPay 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @return    设定参数 
	* @return ResponseMessage    返回类型 
	* @throws 
	* @author undoner@gmail.com 
	* @date 2017年2月7日 下午5:03:54 
	*/
	@RequestMapping("/unionpay")  
	@ResponseBody
    public ResponseMessage unionPay(MerchantData merchantMessage,HttpServletRequest request, HttpServletResponse response){    
		responseMessage=new ResponseMessage();
        try {
        	//检查必传参数
        	//doSomeThing();
        	logger.debug(merchantMessage.toString());
        	
        	//校验MD5值
        	if(!payService.isValidMd5Parameter(merchantMessage)){
        		logger.info("签名验证失败");
        		responseMessage.setCode(0);
                responseMessage.setMsg("fail");
                responseMessage.setObject(merchantMessage);
        	}else{
        		String msg = payService.unionPay(merchantMessage);
                responseMessage.setCode(1);
                responseMessage.setMsg("success");
                responseMessage.setObject(msg);
        	}
        	
        } catch (Exception e) {
            responseMessage.setCode(-1);
            responseMessage.setMsg(e.getMessage());
        }
        return responseMessage;
    }  
	
}
