package cn.lsoft.undoner.dao.redis;

import java.util.List;
import java.util.Map;
import java.util.Set;


/** 
* @ClassName: RedisDao 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author undoner@gmail.com
* @date 2017年1月21日 下午2:09:02 
*/
public interface RedisDao {

    /**
     *
     * @Title: hkeys
     * @author lyt
     * @Description: 获取hash集合中的所有key
     * @param @param key
     * @param @return    设定文件
     * @return Set<String>    返回类型
     * @throws
     */
    Set<String> hkeys(final String key);


    /**
     * hvals
     * 获取hash所有value值
     * @param key
     * @return
     */
    List<String> hvals(final String key);

    /**
     *
     * @Title: sismember
     * @author lyt
     * @Description: 判断set集合中是否存在mem内容
     * @param @param key
     * @param @param mem
     * @param @return    设定文件
     * @return boolean    返回类型
     * @throws
     */
    boolean sismember(final String key, final String mem);

    /**
     * 删除set级和中的一个或多个元素
     * @param key
     * @param mem
     */
    void srem(final String key, final String mem);
    /**
     *
     * @Title: scard
     * @author lyt
     * @Description: 获取Set集合中的总数
     * @param @param key
     * @param @return    设定文件
     * @return long    返回类型
     * @throws
     */
    long scard(final String key);

    /**
     *
     * @Title: keys
     * @author lyt
     * @Description: 模糊查询所有匹配patten的key
     * @param @param patten
     * @param @return    设定文件
     * @return Set<String>    返回类型
     * @throws
     */
    Set<String> keys(final String patten);

    /**
     * 向redis添加hash类型数据
     * @param key
     * @param map
     */
    void hmset(final String key, final Map<String, String> map);

    /**
     * 设置key的过期时间
     * @param key
     * @param seconds
     */
    void expire(final String key, final long seconds);

    /**
     * 从redis获取hash数据
     * @param key
     * @return
     */
    Map<String,String> hgetAll(final String key);

    /**
     *
     * @Title: hset
     * @author lyt
     * @Description: 色环之定key中的hkey的value
     * @param @param key
     * @param @param hkey
     * @param @param hval
     * @param @return    设定文件
     * @return String    返回类型
     * @throws
     */
    Boolean hset(final String key, final String hkey, final String hval);

    /**
     *
     * @Title: hget
     * @author lyt
     * @Description: 获取指定key中的hkey的value
     * @param @param key
     * @param @param hkey
     * @param @return    设定文件
     * @return String    返回类型
     * @throws
     */
    String hget(final String key, final String hkey);

    /**
     * @Title: hkeyExist
     * @author lyt
     * @Description: 从redis获取hash的key是否存在
     * @param @param key
     * @param @param hkey
     * @param @return    设定文件
     * @return boolean    返回类型
     * @throws
     */
    boolean hkeyExist(final String key, final String hkey);

    /**
     * 向redis添加set类型数据
     * @param key
     * @param value
     */
    void sadd(final String key, final String value);

    /**
     * 从redis获取set数据
     * @param key
     * @return
     */
    Set<String> smembers(final String key);

    /**
     * 判断redis是否存在key
     * @param key
     * @return
     */
    boolean exists(final String key);

    /**
     * 删除key
     * @param key
     */
    void del(final String key);


    /**
     * 从
     * @param key
     * @param hkey
     */
    void hdel(final String key, final String hkey);

    /**
     * 选择redis数据库
     * @param dbId
     */
    void select(final int dbId);


    /**
     *得到key的个数
     * @param key
     * @return
     */
    long hLen(final String key);


    /**
     * 获取key的值
     * @param key
     * @return
     */
    String get(final String key);


    /**
     * 设置key的value
     * @param key
     * @param val
     */
    void set(final String key, final String val);

    /**
     * 从redis获取hash数据
     * @param key
     * @return
     */
    Map<String,Object> hgetAllObj(String key);


    //list操作

    /**
     * 从list头部添加一个元素
     * @param key
     * @param val
     */
    void lpush(final String key, final String val);

    /**
     * 从list尾部添加一个元素
     * @param key
     * @param val
     */
    void rpush(final String key, final String val);


    /**
     * 返回列表key的长度
     * @param key
     * @return
     */
    long llen(final String key);


    /**
     * list分页查询
     * @param key
     * @param start
     * @param end
     * @return
     */
    List<String > lrange(final String key, final Long start, final Long end);


    /**
     * list更新下标index的数据
     * @param key
     * @param index
     * @param val
     */
    void  lset(final String key, final Long index, final String val);


    /**
     * 返回下标为index的value
     * @param key
     * @param index
     * @return
     */
    String lindex(final String key, final Long index);

    /**
     * 判断redis是否存在key
     * @param key
     * @return
     */
    boolean keyExists(final String key);

    /**
     * 向redis添加set类型数据
     * @param key
     * @param value
     */
    void sAdd(final String key,final String value);

    /**
     * 从redis获取hash数据
     * @param key
     * @return
     */
    Map<String,String> hGetAll(final String key);


}
