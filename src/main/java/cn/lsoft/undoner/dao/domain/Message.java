package cn.lsoft.undoner.dao.domain;

import java.io.Serializable;
import java.util.Date;

public class Message implements Serializable {
    private Integer id;

    private String requestUuid;

    private String requestContent;

    private Integer requestStatus;

    private Date requestTime;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRequestUuid() {
        return requestUuid;
    }

    public void setRequestUuid(String requestUuid) {
        this.requestUuid = requestUuid == null ? null : requestUuid.trim();
    }

    public String getRequestContent() {
        return requestContent;
    }

    public void setRequestContent(String requestContent) {
        this.requestContent = requestContent == null ? null : requestContent.trim();
    }

    public Integer getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(Integer requestStatus) {
        this.requestStatus = requestStatus;
    }

    public Date getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(Date requestTime) {
        this.requestTime = requestTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", requestUuid=").append(requestUuid);
        sb.append(", requestContent=").append(requestContent);
        sb.append(", requestStatus=").append(requestStatus);
        sb.append(", requestTime=").append(requestTime);
        sb.append("]");
        return sb.toString();
    }
}