package cn.lsoft.undoner.dao.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MessageCriteria {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public MessageCriteria() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andRequestUuidIsNull() {
            addCriterion("request_uuid is null");
            return (Criteria) this;
        }

        public Criteria andRequestUuidIsNotNull() {
            addCriterion("request_uuid is not null");
            return (Criteria) this;
        }

        public Criteria andRequestUuidEqualTo(String value) {
            addCriterion("request_uuid =", value, "requestUuid");
            return (Criteria) this;
        }

        public Criteria andRequestUuidNotEqualTo(String value) {
            addCriterion("request_uuid <>", value, "requestUuid");
            return (Criteria) this;
        }

        public Criteria andRequestUuidGreaterThan(String value) {
            addCriterion("request_uuid >", value, "requestUuid");
            return (Criteria) this;
        }

        public Criteria andRequestUuidGreaterThanOrEqualTo(String value) {
            addCriterion("request_uuid >=", value, "requestUuid");
            return (Criteria) this;
        }

        public Criteria andRequestUuidLessThan(String value) {
            addCriterion("request_uuid <", value, "requestUuid");
            return (Criteria) this;
        }

        public Criteria andRequestUuidLessThanOrEqualTo(String value) {
            addCriterion("request_uuid <=", value, "requestUuid");
            return (Criteria) this;
        }

        public Criteria andRequestUuidLike(String value) {
            addCriterion("request_uuid like", value, "requestUuid");
            return (Criteria) this;
        }

        public Criteria andRequestUuidNotLike(String value) {
            addCriterion("request_uuid not like", value, "requestUuid");
            return (Criteria) this;
        }

        public Criteria andRequestUuidIn(List<String> values) {
            addCriterion("request_uuid in", values, "requestUuid");
            return (Criteria) this;
        }

        public Criteria andRequestUuidNotIn(List<String> values) {
            addCriterion("request_uuid not in", values, "requestUuid");
            return (Criteria) this;
        }

        public Criteria andRequestUuidBetween(String value1, String value2) {
            addCriterion("request_uuid between", value1, value2, "requestUuid");
            return (Criteria) this;
        }

        public Criteria andRequestUuidNotBetween(String value1, String value2) {
            addCriterion("request_uuid not between", value1, value2, "requestUuid");
            return (Criteria) this;
        }

        public Criteria andRequestContentIsNull() {
            addCriterion("request_content is null");
            return (Criteria) this;
        }

        public Criteria andRequestContentIsNotNull() {
            addCriterion("request_content is not null");
            return (Criteria) this;
        }

        public Criteria andRequestContentEqualTo(String value) {
            addCriterion("request_content =", value, "requestContent");
            return (Criteria) this;
        }

        public Criteria andRequestContentNotEqualTo(String value) {
            addCriterion("request_content <>", value, "requestContent");
            return (Criteria) this;
        }

        public Criteria andRequestContentGreaterThan(String value) {
            addCriterion("request_content >", value, "requestContent");
            return (Criteria) this;
        }

        public Criteria andRequestContentGreaterThanOrEqualTo(String value) {
            addCriterion("request_content >=", value, "requestContent");
            return (Criteria) this;
        }

        public Criteria andRequestContentLessThan(String value) {
            addCriterion("request_content <", value, "requestContent");
            return (Criteria) this;
        }

        public Criteria andRequestContentLessThanOrEqualTo(String value) {
            addCriterion("request_content <=", value, "requestContent");
            return (Criteria) this;
        }

        public Criteria andRequestContentLike(String value) {
            addCriterion("request_content like", value, "requestContent");
            return (Criteria) this;
        }

        public Criteria andRequestContentNotLike(String value) {
            addCriterion("request_content not like", value, "requestContent");
            return (Criteria) this;
        }

        public Criteria andRequestContentIn(List<String> values) {
            addCriterion("request_content in", values, "requestContent");
            return (Criteria) this;
        }

        public Criteria andRequestContentNotIn(List<String> values) {
            addCriterion("request_content not in", values, "requestContent");
            return (Criteria) this;
        }

        public Criteria andRequestContentBetween(String value1, String value2) {
            addCriterion("request_content between", value1, value2, "requestContent");
            return (Criteria) this;
        }

        public Criteria andRequestContentNotBetween(String value1, String value2) {
            addCriterion("request_content not between", value1, value2, "requestContent");
            return (Criteria) this;
        }

        public Criteria andRequestStatusIsNull() {
            addCriterion("request_status is null");
            return (Criteria) this;
        }

        public Criteria andRequestStatusIsNotNull() {
            addCriterion("request_status is not null");
            return (Criteria) this;
        }

        public Criteria andRequestStatusEqualTo(Integer value) {
            addCriterion("request_status =", value, "requestStatus");
            return (Criteria) this;
        }

        public Criteria andRequestStatusNotEqualTo(Integer value) {
            addCriterion("request_status <>", value, "requestStatus");
            return (Criteria) this;
        }

        public Criteria andRequestStatusGreaterThan(Integer value) {
            addCriterion("request_status >", value, "requestStatus");
            return (Criteria) this;
        }

        public Criteria andRequestStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("request_status >=", value, "requestStatus");
            return (Criteria) this;
        }

        public Criteria andRequestStatusLessThan(Integer value) {
            addCriterion("request_status <", value, "requestStatus");
            return (Criteria) this;
        }

        public Criteria andRequestStatusLessThanOrEqualTo(Integer value) {
            addCriterion("request_status <=", value, "requestStatus");
            return (Criteria) this;
        }

        public Criteria andRequestStatusIn(List<Integer> values) {
            addCriterion("request_status in", values, "requestStatus");
            return (Criteria) this;
        }

        public Criteria andRequestStatusNotIn(List<Integer> values) {
            addCriterion("request_status not in", values, "requestStatus");
            return (Criteria) this;
        }

        public Criteria andRequestStatusBetween(Integer value1, Integer value2) {
            addCriterion("request_status between", value1, value2, "requestStatus");
            return (Criteria) this;
        }

        public Criteria andRequestStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("request_status not between", value1, value2, "requestStatus");
            return (Criteria) this;
        }

        public Criteria andRequestTimeIsNull() {
            addCriterion("request_time is null");
            return (Criteria) this;
        }

        public Criteria andRequestTimeIsNotNull() {
            addCriterion("request_time is not null");
            return (Criteria) this;
        }

        public Criteria andRequestTimeEqualTo(Date value) {
            addCriterion("request_time =", value, "requestTime");
            return (Criteria) this;
        }

        public Criteria andRequestTimeNotEqualTo(Date value) {
            addCriterion("request_time <>", value, "requestTime");
            return (Criteria) this;
        }

        public Criteria andRequestTimeGreaterThan(Date value) {
            addCriterion("request_time >", value, "requestTime");
            return (Criteria) this;
        }

        public Criteria andRequestTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("request_time >=", value, "requestTime");
            return (Criteria) this;
        }

        public Criteria andRequestTimeLessThan(Date value) {
            addCriterion("request_time <", value, "requestTime");
            return (Criteria) this;
        }

        public Criteria andRequestTimeLessThanOrEqualTo(Date value) {
            addCriterion("request_time <=", value, "requestTime");
            return (Criteria) this;
        }

        public Criteria andRequestTimeIn(List<Date> values) {
            addCriterion("request_time in", values, "requestTime");
            return (Criteria) this;
        }

        public Criteria andRequestTimeNotIn(List<Date> values) {
            addCriterion("request_time not in", values, "requestTime");
            return (Criteria) this;
        }

        public Criteria andRequestTimeBetween(Date value1, Date value2) {
            addCriterion("request_time between", value1, value2, "requestTime");
            return (Criteria) this;
        }

        public Criteria andRequestTimeNotBetween(Date value1, Date value2) {
            addCriterion("request_time not between", value1, value2, "requestTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}