package cn.lsoft.undoner.dao.mapper;

import cn.lsoft.undoner.dao.DbMapper;
import cn.lsoft.undoner.dao.domain.Message;
import cn.lsoft.undoner.dao.domain.MessageCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface MessageMapper extends DbMapper {
    int countByExample(MessageCriteria example);

    int deleteByExample(MessageCriteria example);

    int deleteByPrimaryKey(Integer id);

    int insert(Message record);

    int insertSelective(Message record);

    List<Message> selectByExampleWithRowbounds(MessageCriteria example, RowBounds rowBounds);

    List<Message> selectByExample(MessageCriteria example);

    Message selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Message record, @Param("example") MessageCriteria example);

    int updateByExample(@Param("record") Message record, @Param("example") MessageCriteria example);

    int updateByPrimaryKeySelective(Message record);

    int updateByPrimaryKey(Message record);
}