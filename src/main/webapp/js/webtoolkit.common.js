function isEmpty(value) {
	return typeof value === 'undefined' || value === null || value === '' || value !== value;
}

function isNotEmpty(value) {
	return !isEmpty(value);
}
