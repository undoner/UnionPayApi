<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String errMsg = (String)request.getAttribute("errMsg");
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>支付异常</title>
<style type="text/css">
.ws_container_success{
	min-width:320px;
	max-width:1024px;
	margin:10px auto;
	overflow:hidden;
}
.vip_box_buy_success {
	background: #ececec;
	font-family: 微软雅黑;
	padding-bottom: 0px;
}
.vip_box_buy_group_success hgroup{
	padding-top:497px;
	font-size:42px;
}
.GROUP_IMG_SUCCESS{
	margin-top:-59px;
	margin-left:-305px;
}
.vip_box_buy_message_success hmessage{
	font-size:25px;
	color:#686868;
}
.vip_box_buy_message_success{
	padding-top:20px;
}
.vip_box_buy_btn_success{
	background:#F60;
	font-size:36px;
	line-height: 92px;
	text-align: center;
	/*height:97px;*/
	/*margin: 125px 10px;*/
	margin-top:384px;
	margin-left:32px;
	margin-right:32px;
}
a{
	text-decoration:none;
	color: #fff;
}
</style>
<script type="text/javascript">

 //function f_close(){
//            if(typeof WeixinJSBridge != 'undefined'){
//                WeixinJSBridge.call('closeWindow');
//            }else{
//                if (navigator.userAgent.indexOf("MSIE") > 0) {  
//                    if (navigator.userAgent.indexOf("MSIE 6.0") > 0) {  
//                        window.opener = null; window.close();  
//                    }  
//                    else {  
//                        window.open('', '_top'); window.top.close();  
//                    }  
//                }  
//                else if (navigator.userAgent.indexOf("Firefox") > 0) {  
//                    window.location.href = 'about:blank ';  
//                    //window.history.go(-2);  
//                }  
//                else {  
//                    window.opener = null;   
//                    window.open('', '_self', '');  
//                    window.close();  
//                }
//            }
//        }

function logout(){
	 if(typeof WeixinJSBridge != 'undefined'){
                WeixinJSBridge.call('closeWindow');
            }else{
			
		if (navigator.userAgent.indexOf("MSIE") > 0) {  
			if (navigator.userAgent.indexOf("MSIE 6.0") > 0) {  
				window.opener = null; window.close();  
			}  
			else {  
				window.open('', '_top'); window.top.close();  
			}  
		}  
		else if (navigator.userAgent.indexOf("Firefox") > 0) {  
			window.location.href = 'about:blank ';  
			//window.history.go(-2);  
		}  
		else {  
			window.opener = null;   
			window.open('', '_self', '');  
			window.close();  
		}
	}
 }

</script>
</head>

<body>
<div class="ws_container_success">
	<div class="vip_box_buy_success" style="margin:0 10px;">
        <div style="text-align:center;" class="vip_box_buy_group_success">
            <hgroup><%=errMsg%></hgroup>
        </div>
        <div class="vip_box_buy_message_success" style="text-align:center;">
        	<hmessage></hmessage>        
        </div>
        <div class="vip_box_buy_btn_success">
        	<a href="javascript:;" onClick="logout();" style="display:block;" >确定</a>
        </div>
 		<div class="vip_box_buy_btn_bottom" style="background: #ececec;height:30px;"></div>
	</div>
</div>
</body>
</html>
