<%@page import="java.text.SimpleDateFormat"%>
<%@ page contentType="text/html; charset=utf-8"%>
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>二维码统一支付测试</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<%
		SimpleDateFormat sdf = new SimpleDateFormat("ddHHmmss");
		String timeStr = sdf.format(new Date());
		String merchantId = "123456" + timeStr;
	 %>
  </head>
<script type="text/javascript" src="<%=basePath%>/js/webtoolkit.common.js"></script>
<script type="text/javascript" src="<%=basePath%>/js/webtoolkit.md5.js"></script>
<script type="text/javascript">
	function  checkData(){
	//<![CDATA[
		var form = document.forms[0];
		var	nodes = form.children;
		var keys = [], map = {};

		for (i = 0, len = nodes.length; i < len; i++) {
			var key = nodes[i].name;
			if (key == 'signType' || key == 'signValue'|| key == 'submit')
				continue;

			map[key] = nodes[i].value;
			keys.push(key);
		}

		keys.sort();

		var ss = '';
		for (i = 0, len = keys.length; i < len; i++) {
			var k = keys[i], v = map[k];
			if (isNotEmpty(v))
				ss += (k + '=' + v + '&');
		}

		var temp = ss;
		ss = ss.slice(0, ss.length - 1);
		
		var signValue = MD5(ss + 'fe55b3f461394242a95858a8b6cbfd2d');
		document.getElementsByName('signValue')[0].value = signValue;
		
		//alert('date=' + temp+'signType=MD5'+'&signValue=' + signValue)
		setTimeout(function() { form.submit(); }, 10000);
	//]]>
	}
	</script>
  <body>
  <h3>二维码统一支付测试:</h3>
  	<p>本页面生成订单---->返回支付二维码---->手机扫二维码---->支付确认页面---->根据访问来源 调用相应支付接口(支付宝/微信/京东/其他三方等)</p>
   <form action="<%=basePath%>/pay/gateway" method="post"  onsubmit = "checkData();">
   		接口名称(serviceName):<input type="text" name="serviceName" value="gateway"><br>
   		商户编号 (merchantId):<input type="text" name="merchantId" value="<%=merchantId%>"><br>
      	字符编码(inputCharset):<input type="text" name="inputCharset" value="utf-8"><br>
      	支付结果通知URL (returnURL):<input type="text" name="returnURL" value="http://127.0.0.1:8080/UnionPayApi/returnURL"><br>
      	支付异常返回URL (errorURL):<input type="text" name="errorURL" value="http://127.0.0.1:8080/UnionPayApi/errorURL"><br>
      	支付异步通知URL (notifyURL):<input type="text" name="notifyURL" value="http://127.0.0.1:8080/UnionPayApi/notifyURL"><br>
      	商品名称  (productName):<input type="text" name="productName" value="三国演义"><br>
      	交易金额(分为单位)  (tradeMoney):<input type="text" name="tradeMoney" value="9800"><br>
      	公用回传参数(remark) :<input type="text" name="remark" value="回传数据"><br>
      	签名方式(signType):<input type="text" name="signType" value="MD5"/><br/>
		签名内容(signValue):<input type="text" name="signValue" value=""/><br/>
      	<input type="submit" name="submit" value="submit"><br>
   </form>
  </body>
</html>
